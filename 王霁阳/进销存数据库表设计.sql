create table Supplier  /*供应商表*/  

(  
  SupplierID int  primary key identity(1,1), /* 供应商编号 ,主键 */ 
  SupplierName varchar(250) not null, /* 供应商名称 */  
  SupplierAddress  varchar(250)not null, /* 地址 */  
  SupplierPhone  varchar(25) null,     /* 电话 */  
  SupplierConstactPerson varchar(20) null      /* 联系人 */  
 )  
create table Customer   /* 客户表*/  

(  

  CustomerID int primary key identity(1,1), /* 客户编号,主键*/  
  CustomerName varchar(250) not null, /* 客户名称 */  
  CustomerAddress  varchar(250) not null, /* 地址 */   
  CustomerPhone varchar(25) null,/* 电话 */  
  CustomerConstactPerson  varchar(20) null  /* 联系人 */  
 )   
create table  Dept   /*部门表*/
(  
  DeptID int primary key identity(1,1), /* 部门编号,主键 */  
  DeptName varchar(30) not null, /* 名称 */  
  DeptRemark  varchar(250)  not null/* 描述,备注 */   
)  
create table Employee         /* 员工表 */  
(   
  EmployeeID int primary key identity(1,1),  /* 员工编号 */  
  EmployeeDeptID int foreign key(EmployeeDeptID) references Dept(DeptID)  not null,  /* 所属部门编号 */  
  EmployeeName varchar(30)  not null,  /* 姓名 */  
  EmployeeDuty varchar(20)  not null,  /* 职务 */  
  EmployeeGender  varchar(6)   not null,  /* 性别 */  
  EmployeeBirthDate  datetime  not null,  /* 出生日期 */  
  EmployeeHireDate datetime null,      /* 合同签订 日期 */  
  EmployeeMatureDate datetime null,      /* 合同到期日 */ 
  EmployeeIdentityCard varchar(20) null,      /* 身份证号 */  
  EmployeeAddress  varchar(250) null,      /* 住址 */  
  EmployeePhone  varchar(25) null,      /* 电话 */  
)  

create table StoreHouse   /* 仓库表 */  
(  
  StoreHouseID int primary key identity(1,1),  /* 仓库编号,主键 */  
  StoreHouseAddress varchar(250) not null,  /* 地址 */  
  StoreHousePhone varchar(25) null,      /* 电话 */  
  EmployeeID int foreign key(EmployeeID) references Employee(EmployeeID) not null,  /* 仓库保管 ,外键 ( 参照 Employee 表 ) */  
)  

create table ProductClass	/* 商品分类表 */
(  
  ProductClassID int primary key identity(1,1),  /* 商品总分类编号, 主键 */   
  ProductClassName varchar(30) not null,  /* 商品分类名称 */  
  EmployeeID  int foreign key(EmployeeID) references Employee(EmployeeID)  not null,  /* 建分类人 ,外键 ( 参照 Employee 表 )*/  
  ProductClassRemark varchar(250) null,    /* 描述,备注 */  
)  

create table ProductList  /* 商品细分类表 */  
( 
	ProductListID int primary key identity(1,1),		 /* 商品细分类编号,主键 */  
  ProductClassID int foreign key(ProductClassID) references ProductClass(ProductClassID) not null, /* 商品总分类编号, 外键 ( 参照ProductClass 表 ) */  
   EmployeeID  int foreign key(EmployeeID) references Employee(EmployeeID)  not null,  /* 建分类人 ,外键 ( 参照 Employee 表 )*/  
  Name  varchar(30) not null, /* 商品分类名称 */  
 )  

create table Product    /* 商品目录表 */  

(  
  ProductID int primary key identity(1,1),  /* 商品名称编号, 主键 */  
  ProductListID int foreign key(ProductListID) references ProductList(ProductListID) not null, /* 商品细分类编号, 外键 ( 参照 ProductList 表 ) */  
  Name varchar(30) not null, /* 商品名称 */  
  Price money null, /* 参考价格 */  
  EmployeeID int foreign key(EmployeeID) references Employee(EmployeeID)  not null, /* 操作员,   外键 ( 参照 Employee 表 )*/  
  CreateDate  datetime null, /* 创建时间 */ 
)  


create table ProductSupplier  /* 商品--供应商表 */  
(   
  ProductID int foreign key(ProductID) references Product(ProductID)  not null,   /* 商品名称编号,外键( 参照 Product 表  )*/ 
  SupplierID int foreign key(SupplierID) references Supplier(SupplierID) not null /* 供应商编号,外键( 参照 Supplier 表) */ 
)  

create table EnterStock    /* 入库单表 */  
(  
  EnterStockID int primary key identity(1,1),  /* 入库单编号 , 主键 */  
  EnterStockEnterDate datetime   not null, /* 入库时间 */  
  DeptID int  not null, /* 入库部门 ,外键 ( 参照 DEPT 表 )*/  
  StoreHouseID int foreign key(StoreHouseID) references StoreHouse(StoreHouseID) not null, /* 所入仓库 ,外键 ( 参照 StoreHouse 表)*/  
  EmployeeID  int foreign key(EmployeeID) references Employee(EmployeeID)   not null  /* 入库人 ,  外键 ( 参照 Employee 表)*/  
)  
create table LeaveStock  /* 出库单表 */  
(  
  LeaveStockID int primary key identity(1,1),  /* 出库单编号 , 主键*/  
  LeaveStockDate  datetime not null,  /* 出库时间 */   
  StoreHouseID  int  foreign key(StoreHouseID) references StoreHouse(StoreHouseID)   not null,  /* 所出仓库 ,外键 ( 参照 StoreHouse 表)*/
  EmployeeID   int  foreign key(EmployeeID) references Employee(EmployeeID) not null,   /* 出库人 ,外键 ( 参照 Employee 表)*/
  ProductID   int foreign key(ProductID) references Product(ProductID) not null,
  Quantity  int not null,  /* 出库数量 */ 
)  
create table BackSale  /* 退货单表 */  
(  
  BackSaleID int primary key identity(1,1),  /* 退货单编号 , 主键 */  
  BackDate datetime not null,  /* 退货日期  */   
  StoreHouseID  int foreign key(StoreHouseID) references StoreHouse(StoreHouseID) not null,  /* 退入仓库 ,  外键 ( 参照 StoreHouse 表)*/  
  EmployeeID  int foreign key(EmployeeID) references Employee(EmployeeID) not null,  /* 退货人 ,    外键 ( 参照 Employee 表)*/  
  Remark varchar(250) null,       /* 退货原因 */ 
  Quantity  int not null, /* 退货数量 */
)  

create table Sale_Detail  /* 销售明细 ( 验货表 ) */    
(  
  SaleDetailID int primary key identity(1,1),
  ProductID int foreign key(ProductID) references Product(ProductID) not null,  /* 商品编号,主键, 外键 ( 参照 Product 表 ) */    
  Quantity  int not null,  /* 数量 */  
  Price money  not null,  /* 价格 */  
  Discount int   null,       /* 折扣 */  
  EmployeeID  int foreign key(EmployeeID) references Employee(EmployeeID) not null   /* 售货人,   外键 ( 参照 Employee 表)*/  
)  
