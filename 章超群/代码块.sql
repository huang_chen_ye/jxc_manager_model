create database Shopping
go

use Shopping
go


--用户信息表(用户Id,用户联系方式,用户地址,用户姓名)
create table AccountInfo
	(	
		AccountId int primary key  identity (1,1),
		AccountPhone varchar(20) not null,
		AccountAddress varchar(100) not null,
		RealName varchar(20) not null
	)
--货物信息表—订货信息 (仓库id 购买商品名称  商品种类   商品数量  商品销售价格 购买时间 收货地址)
create table Cargo
	(
		WarehouseId int not null,
		CommodityName varchar(20) not null,
		CommodityType varchar(20) not null,
		SellPrice money not null,
		PurchaseTime smalldatetime not null,
		ReceiveAddress varchar(100) not null
	)
--销售记录信息表—用户的每笔支出费用 类别（获得用户偏好进行推荐  用户id 购买商品 购买种类 购买价格 月销售量) 
	create table CommodityRecord 
	(
		AccountId int not null,
		CommodityId int not null,
		CommodityName varchar(20) not null,
		CommodityType varchar(20) not null,
		PurchasePrice smalldatetime not null,
		MonthSellAmount int not null
	)
--仓库表—记录所有货物信息(货物Id方便查找,货物名称,货物种类  商品成本价格  货物存数量)
	create table Warehouse
	(
	WarehouseId int primary key identity (1,1),
	WarehouseName varchar(20) not null,
	WarehouseType varchar(20) not null,
	CommodityCostPrice money not null,
	WarehouseAmount int not null

	)
--库存管理表-记录仓库数据（针对仓库表记录货物信息:  货物id  货物调用时间  货物调用状态（分存储和减少） 货物数量）
	create table WarehouseRecord
	(
		WarehouseId int not null,
		UseTime smalldatetime not null,
		UseState varchar(20) not null,
		WarehouseAmount int not null
	)
--供应商表—记录其的联系方式 地址 货物类别 相关信息(仓库id  供应商id  姓名联系方式 货物仓库地址 )
	create table Provider
	(
		ProviderId int primary key identity (1,1),
		ProviderPhone varchar(30) not null,
		WarehouseId int not null,
		ProviderAddress varchar(60) not null
	) 
--供应商销售记录表—记录销售记录销量（获得供应商的销售信息 来评判信誉排行以及货物热度等 供应商id 销售商品  种类 月销售量 成交金额）
	create table ProviderRecord(
		ProviderId int not null,
		SellCommodity varchar(20) not null,
		CommodityType varchar(30) not null,
		MonthSellAmount int not null,
		MonthSumSellMoney int not null                            
	)




--库存管理表
--仓库表
--客户表
--货物信息表
--销售记录信息表
--供应商表
--供应商销售记录表

 